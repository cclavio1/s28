///////////////#3&4
fetch("https://jsonplaceholder.typicode.com/todos")
.then(data=>data.json())
.then(data=>{
    let titles = data.map(key=> key.title)
    console.log(titles)
})
//////////////#5&6
fetch("https://jsonplaceholder.typicode.com/todos")
.then(data=>data.json())
.then(data=>{
    data.forEach(key=>{
        if(key.completed==true){
        console.log(`title: ${key.title} status: Completed`)
        }else{
        console.log(`title: ${key.title} status: Not Completed`)
        }
    })    
})
//////////////#7
fetch("https://jsonplaceholder.typicode.com/todos",{
    method:"POST",
    headers:{
        "Content-type":"application/json"
    },
    body:JSON.stringify({
        userID:7,
        title:"adventura de lata mercedes",
        completed: false
    })
})
.then(data=>data.json())
.then(data=>{
    console.log(data)
})
//////////////#8
fetch("https://jsonplaceholder.typicode.com/todos/1",{
    method:"PUT",
    headers:{
        "Content-type":"application/json"
    },
    body:JSON.stringify({
        userID:7,
        title:"newest of the newest updates",
        completed: false
    })
})
.then(data=>data.json())
.then(data=>{
    console.log(data)
})
//////////////#9
fetch("https://jsonplaceholder.typicode.com/todos/1",{
    method:"PUT",
    headers:{
        "Content-type":"application/json"
    },
    body:JSON.stringify({
        title:"newest of the newest updates",
        description:"a light novel",
        completed: false,
        dateCompleted:"20220201",
        userId:2
    })
})
.then(data=>data.json())
.then(data=>{
    console.log(data)
})
//////////////#10
fetch("https://jsonplaceholder.typicode.com/todos/1",{
    method:"PATCH",
    headers:{
        "Content-type":"application/json"
    },
    body:JSON.stringify({
        title:"aws",
        userID:1
    })
})
.then(data=>data.json())
.then(data=>{
    console.log(data)
})
//////////////#11

fetch("https://jsonplaceholder.typicode.com/todos/1",{
    method:"PATCH",
    headers:{
        "Content-type":"application/json"
    },
    body:JSON.stringify({
        title:"aws",
        status:"Completed",
        dataCompleted:"20220307"
    })
})
.then(data=>data.json())
.then(data=>{
    console.log(data)
})

//////////////#12
fetch("https://jsonplaceholder.typicode.com/todos/1",{
    method:"DELETE",
    headers:{
        "Content-type":"application/json"
    },
    body:JSON.stringify({
        title:"to delete",
        status:"Completed",
        dataCompleted:"20220307"
    })
})
.then(data=>data.json())
.then(data=>{
    console.log(data)
})